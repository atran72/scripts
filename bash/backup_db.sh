#!/bin/bash

<<DOC
File: backup_db.sh
This script will backup the database and email sys admin status.
Author: Anh Tran <hunganh72-at-gmail-dot-com>
Date: 05-10-2014

Set up cron job (crontab -e) like so:
0 2 * * * /path/to/script
| | | | | |
| | | | | +--> script to run
| | | | +----> day-week (0-6)
| | | +------> month (1-12)
| | +--------> day-month (1-31)
| +----------> hour (0-23)
+------------> minute (0-59)
This will run everyday at 2:00 am
DOC

# config.sh must exist and contains DB login variables!
source config.sh

DATE=`date +%m%d%y`

# ---[ DB CONFIG ]--- #
BACKUP_DIR="/backup"
DUMP=`which mysqldump`
DUMP_OPTIONS="--quick --add-drop-table --add-locks --extended-insert --lock-tables"
DB_NAME="hvl"
DB_USER="$DB_BACKUP_USER"
DB_PASS="$DB_BACKUP_PASS"

# ---[ MAIL CONFIG ]--- #
MAIL=`which mail`
EMAIL_ID="admin@example.com"
BACKUP_OK="DB successfully backed up"
BACKUP_FAIL="DB failed to backup"
TMP_FILE="/tmp/mailbody-$DATE"

# create backup dir if it does not exist
if [ ! -d $BACKUP_DIR ]
then
    mkdir -p $BACKUP_DIR
fi

# Dumping database
$DUMP -u$DB_USER -p$DB_PASS $DUMP_OPTIONS $DB_NAME > $BACKUP_DIR/$DB_NAME-$DATE.sql 2>>$BACKUP_DIR/ERROR

# creating a tar gzipped archive
cd $BACKUP_DIR && tar czf $DB_NAME-$DATE.sql.tgz $DB_NAME-$DATE.sql && rm -f $DB_NAME-$DATE.sql

# ---[ EMAIL CONFIG ]--- #
E=`ls $BACKUP_DIR | grep ERROR`

if [ -s $E ]    # if ERROR file has any data
then
    echo "Something went wrong..." > $TMP_FILE
    echo "Could not backup database." >> $TMP_FILE
    echo "Please check $BACKUP_DIR/ERROR for details" >> $TMP_FILE
    $MAIL -s "$BACKUP_FAIL" $EMAIL_ID < $TMP_FILE
else
    rm -f $BACKUP_DIR/ERROR
    echo "Database has been backed up successfully." > $TMP_FILE
    echo "Attached is the backup." >> $TMP_FILE
    echo "To extract: tar xzvf $DB_NAME-$DATE.sql.tgz" >> $TMP_FILE
    $MAIL -s "$BACKUP_OK" -a $BACKUP_DIR/$DB_NAME-$DATE.sql.tgz $EMAIL_ID < $TMP_FILE
fi
